//
//  ViewController.swift
//  Git.Development environment
//
//  Created by Oleg Kanatov on 2.02.22.
//

import UIKit

class ViewController: UIViewController {
    
    private let myView = View()
    let defaultLocalizer = AMPLocalizeUtils.defaultLocalizer
    
    let dataArray = ["English", "Беларускі", "Русский"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myView.languagePicker.delegate = self
        myView.languagePicker.dataSource = self
        changeTheme()
    }
    
    override func loadView() {
        view = myView
    }
    
    func refreshLabel() {
        
        myView.welcomeLabel.text = defaultLocalizer.stringForKey(key: "welcome_title")
    }
    
    private func changeTheme() {
        
        myView.darkButtonAction = { [weak self] in
            self?.view.window?.overrideUserInterfaceStyle = .dark
        }
        myView.lightButtonAction = { [weak self] in
            self?.view.window?.overrideUserInterfaceStyle = .light
        }
        myView.autoButtonAction = { [weak self] in
            self?.view.window?.overrideUserInterfaceStyle = .unspecified
        }
    }
}

//-------------------------------------------------
// MARK: - UIPickerViewDelegate, UIPickerViewDataSource
//-------------------------------------------------

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let row = dataArray[row]
        
        return row
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if row == 0 {
            defaultLocalizer.setSelectedLanguage(lang: "en")
        }
        if row == 1 {
            defaultLocalizer.setSelectedLanguage(lang: "be-BY")
        }
        if row == 2 {
            defaultLocalizer.setSelectedLanguage(lang: "ru")
        }
        self.refreshLabel()
    }
}
