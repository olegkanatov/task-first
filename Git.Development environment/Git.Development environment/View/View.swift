//
//  View.swift
//  Git.Development environment
//
//  Created by Oleg Kanatov on 3.02.22.
//

import UIKit

class View: UIView {
    
    var lightButtonAction: (() -> Void)?
    var darkButtonAction: (() -> Void)?
    var autoButtonAction: (() -> Void)?
    
    let iconImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "IconImage")
        return image
    }()
    
    var welcomeLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("welcome_title", comment: "")
        return label
    }()
    
    let languagePicker: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()
    
    let stackButton: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .equalSpacing
        stack.spacing = 40
        return stack
    }()
    
    let lightButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Light", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 20)
        button.addTarget(self, action: #selector(lightButtonTap), for: .touchUpInside)
        return button
    }()
    
    let darkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Dark", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 20)
        button.addTarget(self, action: #selector(darkButtonTap), for: .touchUpInside)
        return button
    }()
    
    let autoButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Auto", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 20)
        button.addTarget(self, action: #selector(autoButtonTap), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(named: "BackgroundColor")
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupConstraints() {
        
        iconImage.translatesAutoresizingMaskIntoConstraints = false
        welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
        languagePicker.translatesAutoresizingMaskIntoConstraints = false
        stackButton.translatesAutoresizingMaskIntoConstraints = false
        lightButton.translatesAutoresizingMaskIntoConstraints = false
        darkButton.translatesAutoresizingMaskIntoConstraints = false
        autoButton.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(iconImage)
        self.addSubview(welcomeLabel)
        self.addSubview(languagePicker)
        self.addSubview(stackButton)
        stackButton.addArrangedSubview(lightButton)
        stackButton.addArrangedSubview(darkButton)
        stackButton.addArrangedSubview(autoButton)
        
        NSLayoutConstraint.activate([
            iconImage.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            iconImage.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            
            welcomeLabel.topAnchor.constraint(equalTo: iconImage.bottomAnchor, constant: 20),
            welcomeLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            
            languagePicker.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor),
            languagePicker.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            
            stackButton.topAnchor.constraint(equalTo: languagePicker.bottomAnchor, constant: 20),
            stackButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
        ])
    }
    
    @objc func lightButtonTap() {
        lightButtonAction?()
    }
    
    @objc func darkButtonTap() {
        darkButtonAction?()
    }
    
    @objc func autoButtonTap() {
        autoButtonAction?()
    }
}
